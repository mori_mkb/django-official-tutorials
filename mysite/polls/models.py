import datetime
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone

@python_2_unicode_compatible
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(weeks=0, days=1, hours=0, minutes=0, seconds=0) <= self.pub_date <= now
        #pub_date  =  23 31/6/2017
        #now = 21  2/6/2017
        #time delta = 24 hours
        #now - time delta = 21 1/6/2017
        # 21 1/6/2017 <= 23 31/6/2017 <= 21 1/6/2017  =>>>>>>>>>>>>  it returns False
        # it will only return True if the date is also in the past
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'published recently?'


@python_2_unicode_compatible
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text